import java.io.*;
import java.util.ArrayList;

public class RPN {
    
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        RPN manager = new RPN();

        System.out.println("Welcome to the CS9G RPN simulator!");
        System.out.println("You can type numbers or operators (+, -, /, *, pop, clear, midi, quit)");

        calc.printOperands();
        String argument = manager.getUserInput("Input");

        while (!"quit".equals(argument)) {

            if (!manager.validInput(argument)) {
                System.out.println("Error! " + argument + " is not a number or operator (+, -, /, *, pop, clear, midi, quit)");
            } else if (manager.isNumber(argument)) {
                int value = Integer.parseInt(argument);
                calc.push(value);
            } else if (argument.equals("pop")) {
                calc.pop();
            } else if (argument.equals("clear")) {
                calc.clear();
            } else if (argument.equals("midi")) {
                if (calc.play() == 1) {
                    System.out.println("Error! The stack must have at least 3 numbers for midi to play...");
                }
            } else {
                int op = calc.operate(argument);
                if (op == 1) {
                    System.out.println("Error! The stack must have at least 2 numbers to work...");
                } else if (op == 2) {
                    System.out.println("Error! / by zero.");
                }
            }

            calc.printOperands();
            argument = manager.getUserInput("Input");
        }

    }

    private ArrayList<String> operations;

    // Constructor
    public RPN() {
        operations = new ArrayList<String>();

        operations.add("+");
        operations.add("-");
        operations.add("/");
        operations.add("*");

        operations.add("pop");
        operations.add("clear");
        operations.add("midi");
    }

    public boolean validInput(String input) {
        return (input != null) && (isNumber(input) || operations.contains(input));
    }

    public String getUserInput(String prompt) {
        String inputLine = null;
        System.out.print(prompt + ": ");
        try {
            BufferedReader is = new BufferedReader(new InputStreamReader(System.in));
            inputLine = is.readLine();
            if (inputLine.length() == 0){
                return null;
            }
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }
        return inputLine;
    }

    public boolean isNumber(String argument) {
        try {
            Integer.parseInt(argument);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
}