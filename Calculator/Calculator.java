import java.util.Stack;
import java.util.ArrayList;
import javax.sound.midi.*;
import java.util.regex.*;


public class Calculator {
    
    public static void main(String[] args) {
        Calculator calc = new Calculator();

        calc.push(4);
        calc.printOperands();

        calc.push(1);
        calc.printOperands();

        calc.operate("-");
        calc.printOperands();

        calc.push(2);
        calc.printOperands();

        calc.operate("/");
        calc.printOperands();

        calc.push(0);
        calc.printOperands();

        calc.operate("/");
        calc.printOperands();

        calc.clear();
        calc.printOperands();

        calc.push(10);
        calc.printOperands();

        calc.push(60);
        calc.printOperands();

        calc.push(1);
        calc.printOperands();

        calc.clear();
        calc.printOperands();

    }

    private Stack<Integer> operands;
    private ArrayList<String> operations;
    
    // Constructors.
    public Calculator() {
        operands = new Stack<Integer>();
        operations = new ArrayList<String>();

        operations.add("+");
        operations.add("-");
        operations.add("/");
        operations.add("*");
    }

    public void push(Integer a) {
        operands.push(a);
    }

    public void pop() {
        operands.pop();
    }

    public void clear() {
        operands.clear();
    }

    public int operate(String op) {

        if (operands.size() < 2) {
            return 1;
        }

        int alpha = operands.pop();
        int beta = operands.pop();

        if (op.equals("+")) {
            operands.push(beta + alpha);
            return 0;
        } else if (op.equals("-")) {
            operands.push(beta - alpha);
            return 0;
        } else if (op.equals("/")) {
            if (alpha == 0) {
                operands.push(beta);
                operands.push(alpha);
                return 2;
            } else {
                operands.push(beta / alpha);
                return 0;
            }
        } else if (op.equals("*")) {
            operands.push(beta * alpha);
            return 0;
        } else {
            return 3;
        }
    }

    public int play() {
        if (operands.size() < 3) {
            return 1;
        }

        int instrument = operands.pop();
        int note = operands.pop();
        int duration = operands.pop();

        try {

            Sequencer player = MidiSystem.getSequencer();
            player.open();

            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();

            //MidiEvent event = null;

            ShortMessage first = new ShortMessage();
            first.setMessage(192, 1, instrument, 0);
            MidiEvent changeInstrument = new MidiEvent(first, 1);
            track.add(changeInstrument);

            ShortMessage a = new ShortMessage();
            a.setMessage(144, 1, note, 100);
            MidiEvent noteOn = new MidiEvent(a, 1);
            track.add(noteOn);

            ShortMessage b = new ShortMessage();
            b.setMessage(128, 1, note, 100);
            MidiEvent noteOff = new MidiEvent(b, duration);
            track.add(noteOff);

            System.out.format("Playing MIDI sound: Duration=%d, Note=%d, Instrument=%d...%n", duration, note, instrument);
            player.setSequence(seq);
            player.start();

            Thread.sleep(2000);
            player.close();

        } catch (InvalidMidiDataException mda) {
            String message = mda.getMessage();
            Pattern p = Pattern.compile("(^|\\s)([0-9]+)($|\\s)");
            Matcher m = p.matcher(message);
            m.find();
            int invalid = Integer.valueOf(m.group(2));
            System.out.format("Error! The number %d is outside of the valid MIDI data range...%n", invalid);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();

        } finally {
            operands.push(duration);
            operands.push(note);
            instrument = operands.push(instrument);
        }

        return 0;

    }

    public void printOperands() {
        System.out.println("+- STACK -- top ---------+");

        Stack<Integer> copy = (Stack<Integer>) operands.clone();

        while (!copy.empty()) {
            System.out.println("|                      " + copy.pop() + " |");
        }

        System.out.println("+--------- bottom -------+");
        System.out.println();
    }


}
