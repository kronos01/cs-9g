public class Cat {
    
    // Constructors.
    public Cat ( ) {
	myPosition = new Position ( );
    }
    
    public Cat (Position p) {
	myPosition = p;
    }
    
    // An access function.
    public Position getPosition ( ) {
        return myPosition;
    }
    
    // Move the cat around the statue:
    //	one meter toward the statue if the cat sees the mouse (or up to the statue
    //		if the cat is closer to the statue than one meter away), or 
    //	1.25 meters counterclockwise around the statue if the cat doesn't see the mouse.
    // Return true if the cat eats the mouse during the move, false otherwise.
    public boolean move (Position mousePosition) {
        Position previous = new Position(myPosition);

        if (myPosition.sight(mousePosition)) {
            myPosition.update(-1.0, 0);
        } else {
            myPosition.update(0, 1.25/myPosition.getRadius());
        }

        return (myPosition.track(mousePosition)) && (mousePosition.between(previous, myPosition));
    }
    
    private Position myPosition;
}
