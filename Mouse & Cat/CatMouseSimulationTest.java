public class CatMouseSimulationTest {
    
    // Test simulation
    public static void main(String [] args) {
        double[] catRadius = {1, 3.2, 1, 8.1, 8.1, 4};
        double[] catAngle = {35, 0, 35, 0, 150, 0};
        double[] mouseAngle = {396, -57, 396, 45, 240, -57};

        Position catPosition;
        Position mousePosition;

        Cat cat;
        Mouse mouse;

        CatMouseSimulation sim = new CatMouseSimulation();

        for (int i = 0; i < catRadius.length; i = i + 1) {
            catPosition = new Position(catRadius[i], catAngle[i] * Math.PI/180);
            mousePosition = new Position(1, mouseAngle[i] * Math.PI/180);

            cat = new Cat(catPosition);
            mouse = new Mouse(mousePosition);

            System.out.println();
            System.out.format("catRadius: %16f%n", catRadius[i]);
            System.out.format("catAngle: %16f%n", catAngle[i]);
            System.out.format("mouseAngle: %16f%n", mouseAngle[i]);

            sim.runChase(cat, mouse);
        }

    }
}