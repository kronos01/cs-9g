public class Mouse {
    
    // Constructors.
    public Mouse ( ) {
	myPosition = new Position ( );
    alive = true;
    }
    
    public Mouse (Position p) {
	myPosition = p;
    alive = true;
    }
    
    // An access function.
    public Position getPosition() {
        return myPosition;
    }
    
    // Move the mouse one meter counterclockwise around the statue.
    public void move() {
        if (alive) {
            myPosition.update (0.0, 1.0);
        }
    }

    public void dead() {
        alive = false;
    }

    public boolean isAlive() {
        return alive;
    }
    
    private Position myPosition;
    private boolean alive;
}
