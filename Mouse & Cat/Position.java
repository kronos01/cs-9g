public class Position {
    
    // Represent a position (radius, angle) in polar coordinates.
    // All angles are in radians.
    // The internal representation of an angle is always at least 0
    // and less than 2 * PI.  Also, the radius is always at least 1.
    
    // Constructors.
    public Position ( ) {
	myRadius = 1;
	myAngle = 0;
    }
    
    public Position (Position p) {
	myRadius = p.myRadius;
	myAngle = p.myAngle;
    }
    
    public Position (double r, double theta) {
	myRadius = r;
	myAngle = theta;
    }
    
    // Return a printable version of the position.
    public String toString ( ) {
        double radius = Math.round(myRadius*100.0)/100.0;
        double angle = Math.round(myAngle*100.0)/100.0;

        return "(" + radius + "," + angle + ")";
    }
    
    // Update the current position according to the given increments.
    // Preconditions: thetaChange is less than 2 * PI and greater than -2 * PI;
    //   one of rChange and thetaChange is 0.
    public void update(double rChange, double thetaChange) {
	// You fill this in.
        if (myRadius + rChange < 1) {
            myRadius = 1;
        } else {
            myRadius = myRadius + rChange;
        }

        myAngle = myAngle + thetaChange;
    }

    public boolean track(Position other) {
        return getRadius() == other.getRadius();
    }

    public boolean equals(Position other) {
        return (getRadius() == other.getRadius()) && (Math.cos(getAngle() - other.getAngle()) == 1);
    }

    public boolean between(Position a, Position b) {
        double alpha = a.getAngle();
        double beta = b.getAngle();

        if (beta - alpha <= Math.PI/2){
            return (Math.cos(getAngle() - alpha) > Math.cos(beta - alpha)) && (Math.cos(beta - getAngle()) > Math.cos(beta - alpha));
        } else {
            return false;
        }
    }

    public boolean sight(Position omega) {
        return getRadius() * Math.cos(getAngle() - omega.getAngle()) >= omega.getRadius();
    }

    public double getRadius() {
        return myRadius;    
    }

    public double getAngle() {
        return myAngle;
    }

    private double myRadius;
    private double myAngle;


    // Test position
    public static void main(String [] args) {
        Position a = new Position(1, 0);
        Position b = new Position(2, 0);
        Position c = new Position(4, 0);
        Position d = new Position(8, 0);

        b.update(0, Math.PI/6);
        c.update(0, Math.PI/4);
        d.update(0, Math.PI/3);

        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(c.toString());
        System.out.println(d.toString());

        System.out.println();
        System.out.println("b, c - between");
        System.out.println(b.between(a, c));
        System.out.println(b.between(a, d));
        System.out.println(c.between(b, d));
        System.out.println(c.between(a, d));

        System.out.println();
        System.out.println("a, d - between");
        System.out.println(a.between(b, d));
        System.out.println(d.between(c, a));

        a.update(0, 5 * Math.PI/4);

        System.out.println();
        System.out.println("d-sight");
        System.out.println(d.sight(a));
        System.out.println(d.sight(b));
        System.out.println(d.sight(c));

        System.out.println();
        System.out.println("c-sight");
        System.out.println(c.sight(a));
        System.out.println(c.sight(b));

        System.out.println();
        System.out.println("b-sight");
        System.out.println(b.sight(a));

        System.out.println();
        System.out.println(a.toString());

    }
}
