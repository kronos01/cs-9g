public class CatMouseSimulation {
    
    // Run the simulation
    public static void runChase(Cat cat, Mouse mouse) {
    	int timer = 0;

    	Position catPosition;
    	Position mousePosition;

    	System.out.println("Welcome to the CS9G cat and mouse simulation!");
    	System.out.format("%16s%16s%16s%16s%n", "Time", "Status", "Mouse", "Cat");

    	while (mouse.isAlive() && timer < 30) {
    		catPosition = cat.getPosition();
    		mousePosition = mouse.getPosition();
    		System.out.format("%16s%16s%16s%16s%n", String.valueOf(timer), "running", mousePosition.toString(), catPosition.toString());

    		if (cat.move(mousePosition)) {
    			mouse.dead();
    		}

    		mouse.move();

    		timer = timer + 1;
    	}

    	catPosition = cat.getPosition();
    	mousePosition = mouse.getPosition();

    	if (!mouse.isAlive()) {
    		System.out.format("%16s%16s%16s%16s%n", String.valueOf(timer), "eaten", mousePosition.toString(), catPosition.toString());
    	} else if (timer >= 30) {
    		System.out.format("%16s%16s%16s%16s%n", String.valueOf(timer), "tired", mousePosition.toString(), catPosition.toString());
    	}
    }

    // Set up the arguments and then call runChase to run the simulation
    public static void main(String [] args) {
    	double catRadius = Double.parseDouble(args[0]);
    	double catAngle = Double.parseDouble(args[1]);
    	double mouseAngle = Double.parseDouble(args[2]);

    	Position catPosition = new Position(catRadius, catAngle * Math.PI/180);
    	Position mousePosition = new Position(1, mouseAngle * Math.PI/180);

    	Cat cat = new Cat(catPosition);
    	Mouse mouse = new Mouse(mousePosition);

    	runChase(cat, mouse);
    }
}
