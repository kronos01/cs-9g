public class Life1D {
	private Rule rule;
	private int stepCount;

	public static void main (String [ ] args) {
		Life1D simulation = new Life1D ( );
		simulation.processArgs (args);
		simulation.producePBM ( );
	}

	// Print, in Portable Bitmap format, the image corresponding to the rule and step count
	// specified on the command line.
	public void producePBM ( ) {
		int size = (2*stepCount + 1);
		int[] prevRow = new int[size];
		int[] currRow = new int[size];

		System.err.println ("P1" + " " + size + " " + (stepCount + 1));

		for (int i = 0; i < size; i = i + 1) {
			currRow[i] = 0;
		}

		currRow[stepCount] = 1;

		while (stepCount >= 0) {
			for (int i = 0; i < size; i = i + 1) {
				System.out.print(currRow[i]);
				prevRow[i] = currRow[i];
			}

			currRow[0] = rule.output(0, prevRow[0], prevRow[1]);
			currRow[size - 1] = rule.output(prevRow[size - 2], prevRow[size - 1], 0);

			for (int i = 1; i < (size - 1); i = i + 1) {
				currRow[i] = rule.output(prevRow[i - 1], prevRow[i], prevRow[i + 1]);
			}

			System.out.println();

			stepCount = stepCount - 1;
		}

	}

	// Retrieve the command-line arguments, and convert them to values for the rule number 
	// and the timestep count.
	private void processArgs (String [ ] args) {
		if (args.length != 2) {
			System.err.println ("Usage:  java Life1D rule# rowcount");
			System.exit (1);
		}
		try {
			rule = new Rule (Integer.parseInt (args[0]));
		} catch (Exception ex) {
			System.err.println ("The first argument must specify a rule number.");
			System.exit (1);
		}
		try {
			stepCount = Integer.parseInt (args[1]);
		} catch (Exception ex) {
			System.err.println ("The second argument must specify the number of lines in the output.");
			System.exit (1);
		}
		if (stepCount < 1) {
			System.err.println ("The number of output lines must be a positive number.");
			System.exit (1);
		}
	}
}


class Rule {
	private int[] ruleNumber;

	public Rule (int ruleNum) {
		ruleNumber = new int[8];

		for (int pos = 0; pos < 8; pos = pos + 1) {
			ruleNumber[pos] = (ruleNum >> pos) & 1;
		}
	}

	// Return the output that this rule prescribes for the given input.
	// a, b, and c are each either 1 or 0; 4*a+2*b+c is the input for the rule.
	public int output (int a, int b, int c) {
		return ruleNumber[(4*a) + (2*b) + c];
	}

}