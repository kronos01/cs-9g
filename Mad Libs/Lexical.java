import java.util.ArrayList;

public class Lexical {
	private String category;
	private ArrayList<Integer> positions;
	private ArrayList<String> candidates;

	public Lexical(String type) {
		category = type;
		positions = new ArrayList<Integer>();
		candidates = new ArrayList<String>();
    }

    public void addPosition(Integer position) {
		positions.add(position);
    }

    public void addCandidate(String candidate) {
		candidates.add(candidate);
    }

    public int getPosition(int index) {
		if (index < positions.size()) {
			return positions.get(index);
		} else {
			return -1;
		}
    }

    public String getCandidate(int index) {
		if (index < candidates.size()) {
			return candidates.get(index);
		}  else {
			return "";
		}
    }

    public boolean full() {
		return candidates.size() >= positions.size();
    }

    public int length() {
		return Math.min(candidates.size(), positions.size());
    }

    public void clearCandidates() {
    	candidates.clear();
    }

}