import java.util.ArrayList;

public class MadLibsGame {

	public static void main(String[] args) {
		ArrayList<MadLib> templates = new ArrayList<MadLib>();

		templates.add(new MadLib("The -noun- looks not with its eyes, but with the mind; and therefore is -adjective- Cupid painted -adjective-"));
		templates.add(new MadLib("To thine own self be -adjective-, and it must follow, as the night the day, thou canst not then be false to any -noun-"));
		templates.add(new MadLib("Sorrow is knowledge, that -noun- which knows the most must -verb- the deepest, the tree of -noun- is not the tree of -noun-"));
		templates.add(new MadLib("That -noun- is thickly sown with thorns, and I know no other remedy than to -verb- quickly through them. The longer we have to -verb- our misfortunes, the greater is their power to -verb- us."));
		templates.add(new MadLib("Liberty, as well as honor, man ought to -verb- at the hazard of his life, for without it life is -adjective-."));

		GameHelper helper = new GameHelper();

		int rand = (int) (Math.random() * 5);
		MadLib madlib = templates.get(rand);

		Lexical adjective = madlib.getLexical("-adjective-");
		Lexical verb = madlib.getLexical("-verb-");
		Lexical noun = madlib.getLexical("-noun-");
		String word;

		System.out.println("Welcome to Mad Libs! I'm going to ask you for some lexical categories and when I'm all done, I'll give you the Mad Lib.");

		while (!adjective.full()) {
			word = helper.getUserInput("Enter an adjective");
			adjective.addCandidate(word);
		}

		while (!verb.full()) {
			word = helper.getUserInput("Enter a verb");
			verb.addCandidate(word);
		}

		while (!noun.full()) {
			word = helper.getUserInput("Enter a noun");
			noun.addCandidate(word);
		}

		word = madlib.sentence();

		System.out.println("OK, here's your Mad Lib:");
		System.out.println(word);

	}

}