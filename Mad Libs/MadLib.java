import java.util.ArrayList;
import java.util.Arrays;

public class MadLib {
	private ArrayList<String> sentence;
	private Lexical adjectives;
	private Lexical verbs;
	private Lexical nouns;

	public MadLib(String madlib) {
		sentence = new ArrayList<String>(Arrays.asList(madlib.split("\\s+")));

		adjectives = new Lexical("adjective");
		verbs = new Lexical("verb");
		nouns = new Lexical("noun");

		String current;

		for (int i = 0; i < sentence.size(); i = i+1) {
			current = sentence.get(i).toLowerCase();
			if (current.contains("-adjective-")) {
				adjectives.addPosition(i);
			} else if (current.contains("-verb-")) {
				verbs.addPosition(i);
			} else if (current.contains("-noun-")) {
				nouns.addPosition(i);
			}
        }
    }

    public Lexical getLexical(String type) {
		if (type.equals("-adjective-")) {
			return adjectives;
		} else if (type.equals("-verb-")) {
			return verbs;
		} else if (type.equals("-noun-")) {
			return nouns;
		} else {
			return null;
		}
    }

    public void setLexical(String type) {
    	int position;
    	String candidate;
    	Lexical lexical = getLexical(type);

		for (int j = 0; j < lexical.length(); j = j+1) {
			position = lexical.getPosition(j);
			candidate = lexical.getCandidate(j);
			if (0 <= position){
				sentence.set(position, candidate);
			}
		}
    }

    public void clearLexicals() {
    	adjectives.clearCandidates();
    	verbs.clearCandidates();
    	nouns.clearCandidates();
    }

    public String sentence() {
    	setLexical("-adjective-");
    	setLexical("-verb-");
    	setLexical("-noun-");

    	String value = "";
    	for (String word : sentence) {
    		value = value + word + " ";
    	}

    	clearLexicals();

    	return value;

    }
}