public class MadLibsTestDrive {
	public static void main(String[] args) {
		MadLib madlib = new MadLib("I saw a giant -noun- float across the sky. The -noun- told me to -verb-, but I did not listen. I stood there -adjective- waiting for the -noun-.");

		Lexical adjective = madlib.getLexical("-adjective-");
		Lexical verb = madlib.getLexical("-verb-");
		Lexical noun = madlib.getLexical("-noun-");

		String sentence;

		adjective.addCandidate("angrily");
		verb.addCandidate("fly");
		noun.addCandidate("turtle");
		noun.addCandidate("alien");
		noun.addCandidate("flower");
		sentence = madlib.sentence();
		System.out.println(sentence);
		System.out.println();

		adjective.addCandidate("happily");
		verb.addCandidate("run");
		noun.addCandidate("ufo");
		noun.addCandidate("soldier");
		noun.addCandidate("bus");
		sentence = madlib.sentence();
		System.out.println(sentence);
		System.out.println();

		adjective.addCandidate("jealously");
		verb.addCandidate("code");
		noun.addCandidate("pirate ship");
		noun.addCandidate("boss");
		noun.addCandidate("chariot");
		sentence = madlib.sentence();
		System.out.println(sentence);
		System.out.println();
	}
}