public class MadLib {
	private ArrayList<String> sentence;
	private Lexical adjectives;
	private Lexical verbs;
	private Lexical nouns;

	public MadLib(String madlib) {
		sentence = new ArrayList<String>(madlib.split("\\s+"));
		lexicals = new HashSet<Lexical>();

		adjectives = new Lexical("adjective");
		verbs = new Lexical("verb");
		nouns = new Lexical("noun");

		String current;

		for (int i = 0; i < sentence.size(); i = i+1) {
			current = sentence.get(i);
			if (current.equals("-adjective-")) {
				adjectives.addPositon(i);
			} else if (current.equals("-verb-")) {
				verbs.addPositon(i);
			} else if (current.equals("-noun-")) {
				nouns.addPositon(i);
			}
        }
    }

    public Lexical getLexical(String type) {
		if (type.equals("-adjective-")) {
			return adjectives
		} else if (type.equals("-verb-")) {
			return verbs
		} else if (currrent.equals("-noun-")) {
			return nouns;
		}
    }

    public void setLexical(String type) {
    	int position;
    	String candidate;
    	Lexical lexical = getLexical(type);

		for (int j = 0; j < lexical.length(); j = j+1) {
			position = adjectives.getPosition(j)
			candidate = adjectives.getCandidate(j)
			sentence.set(position, candidate)
		}
    }

    public void clearLexicals() {
    	adjectives.clearCandidates();
    	verbs.clearCandidates();
    	nouns.clearCandidates();
    }

    public String sentence() {
    	setLexical("-adjective-")
    	setLexical("-verb-")
    	setLexical("-noun-")

    	String sentence = ""
    	for (String word : sentence) {
    		sentence = sentence + word + " "
    	}

    	clearLexicals();

    	return sentence;

    }
}


public class Lexical {
	private String category;
	private ArrayList<Integer> positions;
	private ArrayList<String> candidates;

	public Lexical(String type) {
		category = type;
		positions = new ArrayList<Integer>();
		candidates = new ArrayList<String>();
    }

    public void addPosition(Integer position) {
		positions.add(position);
    }

    public void addCandidate(String candidate) {
		candidates.add(candidate);
    }

    public int getPosition(int index) {
		if (index < positions.length()) {
			return positions.get(index);
		}
    }

    public String getCandidate(int index) {
		if (index < candidates.length()) {
			return candidates.get(index);
		}
    }

    public boolean full() {
		return candidates.size() >= positions.size();
    }

    public int length() {
		return min(candidates.size(), positions.length());
    }

    public int length() {
		return candidates.size();
    }

    public clearCandidates() {
    	candidates.clear();
    }

}